"""
该脚本会获取Houdini所有节点参数名称、参数标签、节点类型，并将数据
存入工程目录下的node_info.csv文件中

"""

import os
import hou
import numpy as np
import pandas as pd

node_types = hou.nodeTypeCategories()  # 返回的一个字典
'''
{'Chop': <hou.NodeTypeCategory for Chop>, 'ChopNet': <hou.NodeTypeCategory for ChopNet>, 'Cop2': <hou.NodeTypeCategory for Cop2>, 'CopNet': <hou.Nod
eTypeCategory for CopNet>, 'Director': <hou.NodeTypeCategory for Director>, 'Dop': <hou.NodeTypeCategory for Dop>, 'Driver': <hou.NodeTypeCategory f
or Driver>, 'Lop': <hou.NodeTypeCategory for Lop>, 'Manager': <hou.NodeTypeCategory for Manager>, 'Object': <hou.NodeTypeCategory for Object>, 'Shop
': <hou.NodeTypeCategory for Shop>, 'Sop': <hou.NodeTypeCategory for Sop>, 'Top': <hou.NodeTypeCategory for Top>, 'TopNet': <hou.NodeTypeCategory fo
r TopNet>, 'Vop': <hou.NodeTypeCategory for Vop>, 'VopNet': <hou.NodeTypeCategory for VopNet>}
'''

all_nodes = []  # 所有节点名称
parm_names = []
data_list = []

for nc, nt in node_types.items():
    # nc为Categories各大类名称,nt为各大类的脚本类型
    node_types = nt.nodeTypes()  # 获取各大类下的所有节点(字典)
    node_names = list(node_types.keys())  # 该节点类型下所有节点名称
    for name in node_names:
        all_nodes.append(name)  # 记录所有节点
        node = hou.nodeType(nt, name)
        plg = node.parmTemplateGroup()
        f_parms = [p for p in plg.entries()]  # 获取每个最高层级参数
        i_parms = [u for u in plg.entriesWithoutFolders()]  # 获取非目录参数
        f_parms.extend(i_parms)
        parms = list(set(f_parms))

        for i in parms:
            p_name = i.name()  # 参数名称
            p_label = i.label() if i.label() else p_name  # 当不存在标签时，就用参数名称
            if p_name not in parm_names:
                parm_names.append(p_name)
                per_list = [p_name, p_label, name, nc]
                data_list.append(per_list)

print('all_nodes:', len(all_nodes))
print('parm_names:', len(parm_names))

data = np.array(data_list)
df = pd.DataFrame(data, columns=['parm_name', 'parm_label', 'nodename', 'category'])
save_path = os.path.join(hou.getenv('HIP'), 'node_info.csv')
df.to_csv(save_path)
