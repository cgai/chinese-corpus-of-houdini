# Houdini中文语料

#### 介绍
Houdini软件的中文语料

fixed.json: 节点参数标签与其翻译的大Json

get_data.py：
    该脚本会获取Houdini所有节点参数名称、参数标签、节点类型，并将数据
    存入工程目录下的node_info.csv文件中

translate_node.py:
    对选择的节点使用该脚本，会复制一个新的该节点，并对新节点进行参数标签的翻译，
    新节点会多出一个Spare目录

node_info.csv:  
  包含节点参数名称、节点参数标签、所在节点名称、所在节点类型 
  
toolbar:
    translate.shelf 包含上述两个脚本的工具架。放在文档的Houdini18.5的toolbar下即可使用。
    
    
#### 工具安装

1.下载toolbar中的translate.shelf,将该文件放入 文档/Houdini18.5/toolbar下  
![](imgs/1.png)
    
2.打开Houdini在工具栏上添加一个名叫Test的工具栏  
![](imgs/2.png)
    
3.里面会有一个叫Translate的工具，选择你要翻译的节点，点击该工具。  
![](imgs/3.png)

4.之后会自动创建一个新的同类节点，该新节点的Spare里是翻译后的参数栏  
![](imgs/4.png)
    
    
 